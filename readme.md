# Thing
v1.3.0
Thing is a programming language made by gnu/nobody in 2018. It is a stack-based golfing language.
[Thing website](https://mysite---nobody-.repl.co/thing/thing.html)
If you have questions about the language, you can ask me any time. I'm Nobody#4620 on Discord and gnu/nobody on PPCG SE.
Thing is currently in development and new functions will be added with updates.
## How to run your Thing code
1. If you do not have Python 3, install it
2. Download the repo
3. Open your command line
4. Navigate to the folder with Thing in it.
5. Run thing.py with the code you want to run, 1 if you want debug, something else (preferably 0) if not, and the input.
6. That's it!