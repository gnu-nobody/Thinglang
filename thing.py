import sys,ast,math
def parens(s):
    stack=[]
    r=[]
    for i,c in enumerate(s):
        if c=="[":
            stack.append(i)
        elif c=="]":
            r.append((stack.pop(),i))
    return sorted(r)
#the interpreter function
def interpret(s,inp,debug):
	j=0
	#indices of loop brackets
	p=parens(s)
	opens=[t[0]for t in p]
	closes=[t[1]for t in p]
	#the language runs on a stack
	stack = []  
	#i is the index of the character which is currently ran
	i = 0 
	#numbers evaluate to themselves
	nums = "0123456789"  
	#logical false things
	falses=[0,[]]
	#the loop, iterating through the code.
	while i in range(len(s)):
		#getting the character at the index
		c = s[i]
		if debug=="1":
		    print(i,c,stack)
		#numbers evaluate to themselves.
		#all numbers push themselves onto the stack
		if c in nums:
			stack.append(int(c))
			i += 1
		#pushes inp[j] from stdin as integer onto the stack
		elif c == "I":
			stack.append(int(inp[j]))
			i += 1
			j+=1
		#pops the stack and discards the value
		elif c == "p":
			if len(stack) >= 1:
				stack.pop()
				i += 1
			#if the stack is empty,does nothing
			else:
				i += 1
		#pushes inp[j] from stdin as string onto the stack
		elif c == "i":
			stack.append(inp[j])
			i += 1
			j+=1
		#converts the top of the stack to the corresponding
		#ascii character. if it is not an integer, exits
		#with an error
		elif c=="c":
			if len(stack)>=1:
				stack.append(chr(stack.pop()))
				i+=1
			else:
				i+=1
		#converts the top of the stack to an integer. if #it is not a char, gives error 
		elif c=="C":
			if len(stack)>=1:
				stack.append(ord(stack.pop()))
				i+=1
			else:
				i+=1
		#pops two values from the stack and adds them.
		elif c=="+":
			if len(stack)>1:
				stack.append(stack.pop()+stack.pop())
				i+=1
			else:
				i+=1
		#subtraction
		elif c=="-":
			if len(stack)>1:
				stack.append(stack.pop()-stack.pop())
				i+=1
			else:
				i+=1
		elif c=="*": #multiplication
			if len(stack)>1:
				stack.append(stack.pop()*stack.pop())
				i+=1
			else:
				i+=1
		#division. returns a float.
		elif c=="/": 
			if len(stack)>1:
				stack.append(stack.pop()/stack.pop())
				i+=1
			else:
				i+=1
		elif c=="%": #modulo
			if len(stack)>1:
				stack.append(stack.pop()%stack.pop())
				i+=1
			else:
				i+=1
		elif c=="P": #power
			if len(stack)>1:
				stack.append(stack.pop()**stack.pop())
				i+=1
			else:
				i+=1
		elif c=="L": #length of the top of the stack
			if len(stack)>=1:
				stack.append(len(stack.pop()))
				i+=1
			else:
				i+=1
		elif c=="l": #pushes length of the stack 
			stack.append(len(stack))
			i+=1
		#duplicates the top value of the stack
		elif c=="d":
			if len(stack)>=1:
				stack.append(stack[-1])
				i+=1
			else:
				i+=1
		#swaps the top two values of the stack
		elif c=="s":
			if len(stack)>=2:
				a=stack.pop()
				b=stack.pop()
				stack.append(a)
				stack.append(b)
				i+=1
			else:
				i+=1
#puts the second item on the stack without removing it
		elif c=="o":
			if len(stack)>=2:
				stack.append(stack[-2])
				i+=1
			else:
				i+=1
		#rotates the top 3 items on the stack clockwise
		elif c=="r":
			if len(stack)>=3:	
				a=stack.pop()
				b=stack.pop()
				c=stack.pop()
				stack.append(a)
				stack.append(c)
				stack.append(b)
				i+=1
			else:
				i+=1
#rotates the top 3 items on the stack counterclockwise
		elif c=="R":
			if len(stack)>=3:	
				a=stack.pop()
				b=stack.pop()
				c=stack.pop()
				stack.append(b)
				stack.append(a)
				stack.append(c)
				i+=1
			else:
				i+=1
		#listifies the last element
		elif c=="A":
			if len(stack)>=1:
				stack.append([stack.pop()])
				i+=1
			else:
				i+=1
	#appends the second value at the end of the top value
		elif c=="a":
			if len(stack)>=2:
				stack[-1].append(stack[-2])
				del stack[-2]
				i+=1
			else:
				i+=1
		#logical not
		elif c=="$":
			if len(stack)>=1:
				a=stack.pop()
				if a in falses:
					stack.append(1)
				else:
					stack.append(0)
				i+=1
			else:
				i+=1
		#equality test
		elif c=="=":
			if len(stack)>=2:
				if stack.pop()==stack.pop():
					stack.append(1)
				else:
					stack.append(0)
				i+=1
			else:
				i+=1
		#greater than test
		elif c==">":
			if len(stack)>=2:
				if stack.pop()>stack.pop():
					stack.append(1)
				else:
					stack.append(0)
				i+=1
			else:
				i+=1
		#less than test
		elif c=="<":
			if len(stack)>=2:
				if stack.pop()<stack.pop():
					stack.append(1)
				else:
					stack.append(0)
			else:
				i+=1
		#bitwise and
		elif c=="&":
			if len(stack)>=2:
				stack.append(stack.pop()&stack.pop())
				i+=1
			else:
				i+=1
		#bitwise or
		elif c=="|":
			if len(stack)>=2:
				stack.append(stack.pop()|stack.pop())
				i+=1
			else:
				i+=1
		#bitwise xor
		elif c=="^":
			if len(stack)>=1:
				stack.append(stack.pop()^stack.pop())
				i+=1
			else:
				i+=1
		#bitwise not
		elif c=="~":
			if len(stack)>=1:
				stack.append(~stack.pop())
				i+=1
			else:
				i+=1
		#first element of the list/string of the top value
		elif c=="h":
			if len(stack)>=1:
				stack.append(stack.pop()[0])
				i+=1
			else:
				i+=1
		#pushes all but last items of the top value
		elif c=="H":
			if len(stack)>=1:
				stack.append(stack.pop()[:-1])
				i+=1
			else:
				i+=1
		#pushes all but first items of the top value
		elif c=="t":
			if len(stack)>=1:
				stack.append(stack.pop()[1:])
				i+=1
			else:
				i+=1
		#pushes the last item of the top value
		elif c=="T":
			if len(stack)>=1:
				stack.append(stack.pop()[-1])
				i+=1
			else:
				i+=1
		#jump past the matching ] if stack's top value is false 
		elif c=="[":
			if len(stack)>=1:
				if stack.pop() in falses:
					i=closes[opens.index(i)]+1
				else:
					i+=1
			else:
				i+=1
		#jump back to the matching [ if stack's top value is true
		elif c=="]":
			if len(stack)>=1:
				if stack.pop() not in falses:
					i=opens[closes.index(i)]
				else:
					i+=1
			else:
				i+=1
		#subscription s[i] in python
		elif c=="S":
			if len(stack)>=2:
				stack.append(stack.pop()[stack.pop()])
				i+=1
			else:
				i+=1
		#immediately ends the program
		elif c=="e":
			return stack
		#splits the top of the stack by the second value of the stack
		elif c=="W":
			if len(stack)>=2:
				stack.append(stack.pop().split(stack.pop()))
				i+=1
			else:
				i+=1
		#ast.literal_eval. used when parsing inputs etc
		elif c=="q":
			if len(stack)>=1:
				stack.append(ast.literal_eval(stack.pop()))
				i+=1
			else:
				i+=1
		#logarithm
		elif c=="G":
			if len(stack)>=2:
				stack.append(math.log(stack.pop(),stack.pop()))
				i+=1
			else:
				i+=1
		#floor
		elif c=="f":
			if len(stack)>=1:
				stack.append(math.floor(stack.pop()))
				i+=1
			else:
				i+=1
		#containment. 1 if top value is in second, 0 if not
		elif c=="z":
			if len(stack)>=2:
				stack.append(1 if stack.pop()in stack.pop() else 0)
				i+=1
			else:
				i+=1
		#char literal.the next character will be pushed.
		elif c=="\\":
			stack.append(s[i+1])
			i+=2
		#reverse:
		elif c=="K":
			if len(stack)>=1:
				stack.append(stack.pop()[::-1])
			i+=1
	return stack
print(' '.join(map(str,interpret(sys.argv[1],sys.argv[3:],sys.argv[2]))))